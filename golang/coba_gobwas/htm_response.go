package main

import (
	"io"
	"os"
	"strconv"
	"time"
)

const (
	hdrs0      = "HTTP/1.1 200 OK\r\nDate: "
	timeFormat = "Mon, 02 Jan 2006 15:04:05 GMT"
	hdrs1      = "\r\nContent-Type: text/html; charset=utf-8\r\nContent-Length: "
	hdrs2      = "\r\n\r\n"
)

var (
	_len     = len(hdrs0 + timeFormat + hdrs1 + hdrs2)
	notFound = []byte("HTTP/1.1 404 Not Found\r\n\r\n")
)

func HtmRspnHdr(contentLen int) (ret []byte, l int) {
	contentLenStr := strconv.Itoa(contentLen)
	ret = make([]byte, _len+len(contentLenStr)+contentLen)
	l = copy(ret[l:], hdrs0)
	l += copy(ret[l:], time.Now().Format(timeFormat))
	l += copy(ret[l:], hdrs1)
	l += copy(ret[l:], contentLenStr)
	l += copy(ret[l:], hdrs2)
	return
}
func HtmFileRspn(htmFileNm string) []byte {
	file, err := os.Open(htmFileNm)
	if err != nil {
		println(err.Error())
	}
	defer file.Close()
	info, _ := file.Stat()
	ret, l := HtmRspnHdr(int(info.Size()))
	io.ReadFull(file, ret[l:])
	return ret
}
