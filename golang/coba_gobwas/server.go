package main

import (
	"crypto/tls"
	"net"
)

func Server(port, idleWorkerNum int, tlsConfig *tls.Config, handler func(*net.TCPConn, interface{})) {
	addr := new(net.TCPAddr)
	addr.Port = port
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		println("a", err.Error())
		return
	}
	defer listener.Close()
	if tlsConfig != nil {
		listener = tls.NewListener(listener, tlsConfig).(*net.TCPListener)
	}
	println("server start listen on address: all, port:", port)
	var idleWorker func()
	idleWorker = func() {
		conn, err := listener.AcceptTCP()
		if err != nil {
			println("b", err.Error())
			return
		}
		go idleWorker()
		var context interface{}
		handler(conn, context)
	}
	for i := 0; i < idleWorkerNum; i++ {
		go idleWorker()
	}
	<-make(chan struct{})
}
