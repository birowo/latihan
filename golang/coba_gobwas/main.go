package main

import (
	"runtime"
)

func main() {
	port := 8080
	idleWorkerNum := runtime.NumCPU()
	Server(port, idleWorkerNum, nil, handler(HtmFileRspn("client/index.html")))
}
