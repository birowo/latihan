package main

import (
	"errors"
	"io"
	"net"
	"sync"

	"github.com/gobwas/ws"
)

func handler(idxHtmRes []byte) func(*net.TCPConn, interface{}) {
	var mtx sync.Mutex
	u := ws.Upgrader{}
	conns := map[*net.TCPConn]struct{}{}
	return func(conn *net.TCPConn, _ interface{}) {
		u.OnRequest = func(uri []byte) error {
			switch string(uri) {
			case "/":
				conn.Write(idxHtmRes)
			case "/ws":
				return nil
			default:
				conn.Write(notFound)
			}
			conn.Close()
			return errors.New(string(uri))
		}
		_, err := u.Upgrade(conn)
		if err != nil {
			println(err.Error())
			return
		}
		mtx.Lock()
		conns[conn] = struct{}{}
		mtx.Unlock()
		defer func() {
			mtx.Lock()
			delete(conns, conn)
			mtx.Unlock()
			conn.Close()
		}()
		for {
			header, err := ws.ReadHeader(conn)
			if err != nil {
				// handle error
				println(err.Error())
				return
			}
			if header.OpCode == ws.OpClose {
				return
			}
			payload := make([]byte, header.Length)
			_, err = io.ReadFull(conn, payload)
			if err != nil {
				// handle error
			}
			if header.Masked {
				ws.Cipher(payload, header.Mask, 0)
			}

			// Reset the Masked flag, server frames must not be masked as
			// RFC6455 says.
			header.Masked = false
			for _conn, _ := range conns {
				if err := ws.WriteHeader(_conn, header); err != nil {
					// handle error
				}
				if _, err := _conn.Write(payload); err != nil {
					// handle error
				}
			}
		}
	}
}
