package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
)

func Query(retRow func(fieldPtrs []interface{}, isNextRow bool), db *sql.DB, query string, args ...interface{}) (err error) {
	var rows *sql.Rows
	rows, err = db.Query(query, args...)
	if err != nil {
		return
	}
	defer rows.Close()
	fields, _ := rows.Columns()
	fldsLen := len(fields)
	fieldPtrs := make([]interface{}, fldsLen)
	for i := 0; i < fldsLen; i++ {
		fieldPtrs[i] = new(sql.RawBytes)
	}
	isNextRow := rows.Next()
	for isNextRow {
		err = rows.Scan(fieldPtrs...)
		if err != nil {
			return
		}
		isNextRow = rows.Next()
		retRow(fieldPtrs, isNextRow)
	}
	err = rows.Err()
	return
}

type Field struct {
	Name   []byte
	Format [2]byte
}
type Queries []func([]byte) []byte
type JSONWriter struct {
	write      func([]byte) (int, error)
	keys       []Field
	isFirstRow bool
	queries    Queries
}

func (w *JSONWriter) Write(resultPtrs []interface{}, isNextRow bool) {
	var val []byte
	if w.isFirstRow {
		w.write([]byte("[{")) //open first row
	}
	w.isFirstRow = false
	queryIdx := 0
	rsltIdx := 0
	isNextField := false
	for _, key := range w.keys {
		frmt := key.Format
		if frmt[0] == '!' {
			if frmt[1] == '-' {
				val = w.queries[queryIdx](nil)
			} else {
				val = w.queries[queryIdx](*resultPtrs[rsltIdx].(*sql.RawBytes))
				rsltIdx++
			}
			queryIdx++
		} else {
			val = *resultPtrs[rsltIdx].(*sql.RawBytes)
			rsltIdx++
		}
		if val == nil {
			if frmt[1] != '~' {
				if isNextField {
					w.write([]byte(","))
				}
				isNextField = true
				w.write([]byte(`"`))
				w.write(key.Name)
				w.write([]byte(`":null`))
			}
		} else {
			if isNextField {
				w.write([]byte(","))
			}
			isNextField = true
			w.write([]byte(`"`))
			w.write(key.Name)
			w.write([]byte(`":`))
			switch frmt[0] {
			case 's':
				w.write([]byte(`"`))
				w.write(val)
				w.write([]byte(`"`))
			case 'b':
				if val[0] == '1' {
					w.write([]byte("true"))
				} else {
					w.write([]byte("false"))
				}
			default:
				w.write(val)
			}
		}
	}
	if isNextRow {
		w.write([]byte("},{"))
	} else {
		w.write([]byte("}]")) //close last row
	}
}

func main() {
	db, err := sql.Open("mysql", "root:@/")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer db.Close()

	query := `SELECT 123, "ab", true UNION VALUES (null, "cd", false), (0, "ef", null)`

	type Obj struct {
		Field0 int    `json:"field0"`
		Field1 *int   `json:"field1,omitempty"`
		Field2 string `json:"field2"`
		Field3 *bool  `json:"field3"`
	}

	http.HandleFunc("/contoh0", func(w http.ResponseWriter, r *http.Request) {
		//implementasi mengikuti cara yang umum dipakai
		rows, err := db.Query(query)
		if err != nil {
			fmt.Println("0", err)
			return
		}
		defer rows.Close()
		//dari sql query rows ke slice of struct(model) object ke http response dalam bentuk json
		var objs []Obj
		for rows.Next() {
			var obj Obj
			obj.Field0 = -1
			//dari query rows ke struct(model) object
			err = rows.Scan(&obj.Field1, &obj.Field2, &obj.Field3)
			if err != nil {
				fmt.Println("1", err)
				return
			}
			objs = append(objs, obj)
		}
		w.Header().Add("Content-Type", "application/json")
		//dari struct(model) object ke http response dalam bentuk json
		if json.NewEncoder(w).Encode(objs) != nil {
			fmt.Println("2", err)
		}
	})

	flds := []Field{
		{[]byte("field0"), [2]byte{'!', '-'}}, //'!': call subquery, '-': no passing result from parent query
		{[]byte("field1"), [2]byte{0, '~'}},   //0: bukan string atau bool , '~': omitempty
		{[]byte("field2"), [2]byte{'s'}},      //'s': string (mmberikn pengapit " diantara nilai)
		{[]byte("field3"), [2]byte{'b'}},      //'b': bool (mengubah: 0 ke false & 1 ke true)
	}

	http.HandleFunc("/contoh1", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		//Di /contoh1 ini :
		//Implementasi pakai cara kami yaitu langsung dari sql query rows ke http response dalam bentuk json ,
		//tanpa lewat struct / model object .
		subqueries := Queries{func(_ []byte) []byte { return []byte(strconv.Itoa(-1)) }}
		err = Query((&JSONWriter{w.Write, flds, true, subqueries}).Write, db, query)
		if err != nil {
			fmt.Println(err)
			return
		}
		//
		//Sedangkan di /contoh0 di atas :
		//Sebetulnya kembalian sql query dalam bentuk sql.RawBytes ([]byte) ,
		//jika kemudian ditaruh ke struct object maka oleh rows.Scan(...) dilakukan proses konversi dari
		//type []byte ke type dari masing - masing field / property di struct object tsb. , kemudian
		//oleh json encode dilakukan proses konversi kembali ke type []byte sebelum response ke client
		//cara ini tentu kurang efisien / performa dibandingkan dengan yang di /contoh1
	})

	http.ListenAndServe(":8080", nil)
}

//view result at https://gitlab.com/birowo/latihan/blob/master/golang/SqlRowsDirectlyToHttpJsonResponse/httpResponseJsonSqlRows.png
