package main

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/rs/xid"
)

func main() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.Handle("/upload/", http.StripPrefix("/upload/", http.FileServer(http.Dir("upload"))))
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
			return
		}
		name := r.FormValue("name") //get file name, e.g. for stored in database
		fmt.Println(name)
		idStr := xid.New().String()
		file, err := os.OpenFile("upload/"+idStr, os.O_RDONLY|os.O_CREATE, 0644)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()
		r.Body = http.MaxBytesReader(w, r.Body, 2*1024*1024)
		_, err = io.Copy(file, r.Body)
		if err != nil {
			http.Error(w, "bad request", http.StatusBadRequest)
			fmt.Println(err)
			return
		}
		w.Write([]byte(idStr))
	})
	http.ListenAndServe(":8080", nil)
}
