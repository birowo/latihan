package main

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func root(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write([]byte(`
<html>
<head></head>
<body>
<form method="POST" action="/setTimeout" target="iframe1">
	timeout: <select name="timeout">
		<option>9</option>
		<option>1</option>
	</select> second<br>
	<input type="submit">
</form>
<iframe src="javascript:" name="iframe1"></iframe>
</body>
</html>	
	`))
}

const (
	delay   = 2 //2 second
	latency = 1 //assumption latency is 1 second
)

func setTimeout(ctx *context.Context, cancel *context.CancelFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if *cancel != nil {
			(*cancel)()
		}
		timeout, _ := strconv.Atoi(r.PostFormValue("timeout"))
		*ctx, *cancel = context.WithTimeout(context.TODO(), time.Duration(timeout)*time.Second)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Write([]byte(fmt.Sprintf(`
<a href="/hello?t=%d">click me</a>
<div id="counter"></div>
<script>
	var i = %d
	function dec(){
		setTimeout(dec, 1000)
		if(i > 0){
			counter.textContent = i
			i--
		}else{
			counter.textContent = 'timeout'
		}
	}
	dec()
</script>
`, time.Now().Unix(), timeout-delay-latency)))
	}
}
func hello(ctx *context.Context, cancel *context.CancelFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if *ctx == nil || *cancel == nil {
			return
		}
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Println("server: hello handler started")
		defer fmt.Println("server: hello handler ended")
		select {
		case <-time.After(delay * time.Second):
			fmt.Fprintf(w, "hello")
			(*cancel)() //cancel must be called to prevent memory leak
		case <-(*ctx).Done():
			err := (*ctx).Err()
			fmt.Println("server:", err)
			internalError := http.StatusInternalServerError
			http.Error(w, err.Error(), internalError)
			(*cancel)()
		}
	}
}

func main() {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	http.HandleFunc("/", root)
	http.HandleFunc("/setTimeout", setTimeout(&ctx, &cancel))
	http.HandleFunc("/hello", hello(&ctx, &cancel))
	http.ListenAndServe(":8080", nil)
}