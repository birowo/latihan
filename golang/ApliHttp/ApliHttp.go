package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/pgxpool"
	"github.com/rs/xid"
	"gitlab.com/birowo/base64/lib"
	"gitlab.com/birowo/rute/lib"
	"gitlab.com/birowo/sessionredis/lib"
	"golang.org/x/crypto/bcrypt"
	"lukechampine.com/frand"
)

func getBodyErr(w http.ResponseWriter, r *http.Request, max int64, keys ...string) func(...*string) bool {
	r.Body = http.MaxBytesReader(w, r.Body, max)
	return func(pVals ...*string) bool {
		if r.ParseForm() != nil {
			return true
		} else {
			for i, pv := range pVals {
				*pv = r.PostForm.Get(keys[i][1:])
				if keys[i][0] == '*' && *pv == "" {
					return true
				}
			}
		}
		return false
	}
}

const (
	usrIdBgn  = 0
	userIdLen = 16
	usrIdEnd  = usrIdBgn + userIdLen
	csrfBgn   = usrIdEnd
	csrfLen   = 32
	csrfEnd   = csrfBgn + csrfLen
)

func main() {
	pgPool, err := pgxpool.Connect(context.Background(), "user=postgres password=p4ssw0rd database=postgres")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer pgPool.Close()
	_, err = pgPool.Exec(context.Background(), `
	CREATE TABLE IF NOT EXISTS users (
		id text NOT NULL,
    	email text NOT NULL,
    	pass_hash text NOT NULL,
    	fname text,
    	lname text,
    	PRIMARY KEY (id),
    	UNIQUE (email)
	)`)
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = pgPool.Exec(context.Background(), `
	CREATE TABLE IF NOT EXISTS  posts (
    	usr_id text NOT NULL,
    	title text NOT NULL,
    	konten text,
    	create_at bigint,
    	category text,
    	PRIMARY KEY (usr_id, title)
	)`)
	if err != nil {
		fmt.Println(err)
		return
	}
	rdsPool := &kukisesi.RdsPool{
		redis.Pool{
			Dial: func() (redis.Conn, error) {
				return redis.Dial("tcp", "localhost:7481")
			},
		},
	}
	defer rdsPool.Close()

	mux := rute.NewMux()
	mux.HandleFunc("GET/signup", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`
<html><head>
</head><body>
<form method="POST"><table>
	<tr><td>email</td><td>
		<div id="msgPH" style="font-size:10pt; color:red"></div>
		<input name="email">
	</td></tr>
	<tr><td>password</td><td><input name="pass"></td></tr>
	<tr><td>first name</td><td><input name="fNm"></td></tr>
	<tr><td>last name</td><td><input name="lNm"></td></tr>
	<tr><td></td><td><input type="submit" value="S I G N U P"></td></tr>
</table></form>
<script>
	var msg = location.hash
	if(msg) msgPH.textContent = msg.substr(1)
</script>
</body></html>		
		`))
	})
	newId := func() (id []byte) {
		id = make([]byte, 16)
		b64.FromByteSlc(id, xid.New().Bytes())
		return
	}
	mux.HandleFunc("POST/signup", func(w http.ResponseWriter, r *http.Request) {
		var email, password, firstName, lastName string
		const maxBodyLen = 499
		if getBodyErr(w, r, maxBodyLen, "*email", "*pass", "*fNm", "*lNm")(&email, &password, &firstName, &lastName) {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		passHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		id := newId()
		_, err = pgPool.Exec(r.Context(),
			"INSERT INTO users(id, email, pass_hash, fname, lname) VALUES($1, $2, $3, $4, $5)",
			id, email, passHash, firstName, lastName,
		)
		if err != nil {
			if err.(*pgconn.PgError).Code == "23505" {
				http.Redirect(w, r, "/signup#email-already-exist", http.StatusFound)
				return
			}
			fmt.Println(err)
			return
		}
		http.Redirect(w, r, "/login", http.StatusFound)
	})
	mux.HandleFunc("GET/login", func(w http.ResponseWriter, r *http.Request) {
		if kukisesi.GetId(r) != "" {
			http.Redirect(w, r, "/beranda", http.StatusFound)
			return
		}
		w.Write([]byte(`
<html><head>
</head><body>
<form method="POST"><table>
	<tr><td></td><td id="msgPH" style="font-size:10pt; color:red"></td></tr>
	<tr><td>email</td><td><input name="email"></td></tr>
	<tr><td>password</td><td><input name="pass"></td></tr>
	<tr><td><input type="submit" value="L O G I N"></td>
	<td><input type="checkbox" name="kl" value="true" >keep login</td></tr>
</table></form>
<script>
	var msg = location.hash
	if(msg) msgPH.textContent = msg.substr(1)
</script>
</body></html>
		`))
	})
	mux.HandleFunc("POST/login", func(w http.ResponseWriter, r *http.Request) {
		if kukisesi.GetId(r) == "" { //if session id not exist
			var email, password, keepLogin string
			const maxBodyLen = 99
			if getBodyErr(w, r, maxBodyLen, "*email", "*pass", " kl")(&email, &password, &keepLogin) {
				http.Error(w, "Bad Request", http.StatusBadRequest)
				return
			}
			var (
				userId, passwordHash []byte
			)
			if pgPool.QueryRow(r.Context(), "SELECT id, pass_hash FROM users WHERE email=$1", email).Scan(&userId, &passwordHash) != nil ||
				bcrypt.CompareHashAndPassword(passwordHash, []byte(password)) != nil {
				http.Redirect(w, r, "/login#invalid-login", http.StatusFound)
				return
			}
			if keepLogin == "true" {
				rdsPool.NewSession(w, userId, time.Now().Add(31536000)) //new session with 1 year expires
			} else {
				rdsPool.NewSession(w, userId, time.Time{}) //new session with session expires
			}
		}
		http.Redirect(w, r, "/beranda", http.StatusFound)
	})
	mux.HandleFunc("GET/beranda", func(w http.ResponseWriter, r *http.Request) {
		ssnId := kukisesi.GetId(r)
		if ssnId == "" { //if no session id
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		userId, err := rdsPool.GetRange(ssnId, usrIdBgn, usrIdEnd)
		if err != nil { //if user id not found in session
			kukisesi.DelId(w)
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		var firstName, lastName string
		err = pgPool.QueryRow(r.Context(), "SELECT fname, lname FROM users WHERE id=$1", userId).Scan(&firstName, &lastName)
		if err != nil { //if user id not found in user table
			rdsPool.Del(ssnId)
			kukisesi.DelId(w)
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		w.Header().Set("Content-Type", "text/html charset=utf-8")
		fmt.Fprintf(w, `
welcome, %s %s 
<form method="POST" action="/logout"><input type="submit" value="LOGOUT"></form>
<a href="/post-form" target="_blank">New Post</a><br>
<a href="/my-post" target="_blank">My Post</a><br>
<a href="/category" target="_blank">Category</a>
		`, firstName, lastName)
	})
	mux.HandleFunc("POST/logout", func(w http.ResponseWriter, r *http.Request) {
		rdsPool.Del(kukisesi.GetId(r))
		kukisesi.DelId(w)
		http.Redirect(w, r, "/login", http.StatusFound)
	})

	const (
		csrfPH    = "{csrf}" //csrf place holder
		csrfPHLen = len(csrfPH)
	)
	newCsrf := func() (token []byte) {
		token = make([]byte, csrfLen)
		frand.Read(token[csrfLen/4:])
		b64.FromByteSlc(token, token[csrfLen/4:])
		return
	}
	postHTM := []byte(`
<form method="POST" action="/post"><table>
<tr><td>category</td><td><input name="ctgry"></td></tr>
<tr><td>title</td><td><input name="ttl"></td></tr>
<tr><td>post</td><td>
	<textarea name="kntn"></textarea>
	<input type="hidden" name="csrf" value="` + csrfPH + `">
</td></tr>
<tr><td><input type="submit" value="P O S T"></td><td></td></tr>
</table></form>		
		`)
	postHTMCsrf, _ := func(rdsPool *kukisesi.RdsPool, htm []byte) (gen func(http.ResponseWriter, *http.Request), err error) {
		last := len(htm) - csrfPHLen
		pos := 0
		for pos < last && string(htm[pos:pos+csrfPHLen]) != csrfPH {
			pos++
		}
		err = nil
		_pos := pos + csrfPHLen
		gen = func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			w.Write(htm[:pos])
			token := newCsrf()
			w.Write(token)
			w.Write(htm[_pos:])
			rdsPool.SetRange(kukisesi.GetId(r), csrfBgn, token)
		}
		return
	}(rdsPool, postHTM)
	mux.HandleFunc("GET/post-form", func(w http.ResponseWriter, r *http.Request) {
		postHTMCsrf(w, r)
	})
	mux.HandleFunc("POST/post", func(w http.ResponseWriter, r *http.Request) {
		session, err := rdsPool.Get(kukisesi.GetId(r))
		if err != nil {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		var title, konten, category string
		const maxBodyLen = 4999
		if getBodyErr(w, r, maxBodyLen, "*ctgry", "*ttl", "*kntn")(&category, &title, &konten) || string(session[csrfBgn:csrfEnd]) != r.PostForm.Get("csrf") {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		_, err = pgPool.Exec(r.Context(), "INSERT INTO posts(usr_id, title, konten, create_at, category) VALUES($1, $2, $3, $4, $5)",
			session[usrIdBgn:usrIdEnd], title, konten, time.Now().Unix(), category)
		if err != nil {
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}
		w.Write([]byte("here"))
	})
	http.ListenAndServe(":8080", mux)
}