package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/klauspost/compress/flate"
)

func main() {
	testHtm, err := os.Open("./static/test.html")
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	defer testHtm.Close()

	writeFlate, err := os.Create("./static/deflate/test.html.deflate")
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	defer writeFlate.Close()
	flateWriter, err := flate.NewWriter(writeFlate, flate.BestCompression)
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	defer flateWriter.Close()
	_, err = io.Copy(flateWriter, testHtm)
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	flateWriter.Flush()

	mutex := &sync.Mutex{}
	cacheSince := time.Now().Format(http.TimeFormat)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	defer watcher.Close()
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					mutex.Lock()
					testHtm.Seek(0, 0)
					writeFlate.Seek(0, 0)
					flateWriter.Reset(writeFlate)
					_, err := io.Copy(flateWriter, testHtm)
					if err != nil {
						fmt.Println("error:", err)
						return
					}
					flateWriter.Flush()
					cacheSince = time.Now().Format(http.TimeFormat)
					mutex.Unlock()
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				fmt.Println("error:", err)
				return
			}
		}
	}()
	err = watcher.Add("./static")
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	testDeflate, err := os.Open("./static/deflate/test.html.deflate")
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	defer testDeflate.Close()

	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("If-Modified-Since") == cacheSince {
			w.WriteHeader(http.StatusNotModified)
			return
		}
		w.Header().Add("Last-Modified", cacheSince)

		w.Header().Add("Content-Type", "text/html; charset=utf-8")

		var err error
		if strings.Contains(r.Header.Get("Accept-Encoding"), "deflate") {
			w.Header().Add("Content-Encoding", "deflate")
			testDeflate.Seek(0, 0)
			_, err = io.Copy(w, testDeflate)
		} else {
			testHtm.Seek(0, 0)
			_, err = io.Copy(w, testHtm)

		}
		if err != nil {
			fmt.Println("error:", err)
		}
	})

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))

	http.ListenAndServe(":8080", nil)
}
