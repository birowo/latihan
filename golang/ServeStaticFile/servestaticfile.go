package main

import (
	"bytes"
	"fmt"
	"net/http"
	"regexp"
)

type Response struct {
	http.ResponseWriter
}

func (w Response) Write(data []byte) (int, error) {
	bs := regexp.MustCompile(`(<a href=)("[^.][^/]*")(>)`).ReplaceAll(data, []byte("$1$2 download=$2$3"))
	bs = bytes.Replace(bs, []byte("</pre>"), []byte(`
<script>
	var lnk = document.getElementsByTagName('a')
	for(var i=0; i<lnk.length; i++){
		if(lnk[i].download){
			lnk[i].click()
		}
	}
</script>
</pre>	
	`), -1)
	return w.ResponseWriter.Write(bs)
}
func inject(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler.ServeHTTP(Response{w}, r)
	})
}
func main() {
	http.Handle("/static/", inject(http.StripPrefix("/static/", http.FileServer(http.Dir("./static")))))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
	}
}
