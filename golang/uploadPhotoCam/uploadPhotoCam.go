package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"sync/atomic"
)

func main() {
	const url = "/upload"
	indexHtm := []byte(`
<html>
<head></head>
<body>
<video id="video">Video stream not available.</video>
<button id="startbutton">Take photo</button>
<canvas id="canvas"></canvas>
<script>
	function startup(width, url){
		var height = width * (3/4);
		video.width = width;
        video.height = height;
        canvas.width = width;
        canvas.height = height;
		navigator.mediaDevices.getUserMedia({video: true, audio: false})
    	.then(function(stream) {
    		video.srcObject = stream;
    		video.play();
    	})
    	.catch(function(err) {
    		console.log("An error occurred: " + err);
    	});
		startbutton.onclick = function(){
			var context = canvas.getContext('2d');
			context.drawImage(video, 0, 0, width, height);
			canvas.toBlob(function(imageBlob) {
				var req = new XMLHttpRequest();
				req.open('POST', url, true);
				req.send(imageBlob);
			})
		}
	}
	startup(320, '` + url + `')
</script>
</body>
</html>
	`)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(indexHtm)
	})
	cntr := int32(0)
	http.HandleFunc(url, func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Create("test" + strconv.Itoa(int(atomic.AddInt32(&cntr, 1))) + ".png")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer f.Close()
		_, err = io.Copy(f, r.Body)
		if err != nil {
			fmt.Println(err)
			return
		}
	})
	http.ListenAndServe(":8080", nil)
}
