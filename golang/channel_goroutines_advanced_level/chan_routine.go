package main

import (
	"fmt"
	"sync"
	"time"
)

type Val struct {
	val int
	ok  bool
}
type Store struct {
	store map[int]int
	//sync.Mutex
	ckeys chan []int
	cvals chan []Val
}

var latency = time.Second //simulate I/O latency
func NewStore() *Store {
	store := map[int]int{}
	ckeys := make(chan []int)
	cvals := make(chan []Val)
	get := func() {
		for {
			keys := <-ckeys
			vals := make([]Val, len(keys))
			for i, key := range keys {
				val, ok := store[key]
				vals[i] = Val{val, ok}
			}
			time.Sleep(latency)
			cvals <- vals
		}
	}
	go get()
	return &Store{store /*, sync.Mutex{}*/, ckeys, cvals}
}

func (s *Store) Set(key, val int) {
	//s.Lock()
	s.store[key] = val
	//time.Sleep(latency)
	//s.Unlock()
}
func (s *Store) Get(keys ...int) (vals []Val) {
	s.ckeys <- keys
	vals = <-s.cvals
	return
}

type KV struct {
	k, v int
}

func NormalSet(store *Store, kvs []KV) { //set keys-vals to store normally(not inside goroutine)
	for _, kv := range kvs {
		store.Set(kv.k, kv.v)
	}
}
func main() {
	store := NewStore()

	kvs := []KV{{2, 3}, {3, 5}, {5, 7}, {7, 11}, {11, 13}, {13, 17}, {17, 19}}
	NormalSet(store, kvs)

	var wg sync.WaitGroup

	wg.Add(len(kvs))
	start := time.Now().Unix()
	for i := 0; i < len(kvs); i++ {
		go func(i int) {
			defer wg.Done()
			k := kvs[i].k
			v := store.Get(k)[0] //solution1: get value from store with given key (inside goroutine)
			if v.ok {
				fmt.Println("routine:", i, " ,key:", k, " ,val:", v.val)
				return
			}
			fmt.Println("routine:", i, " ,key:", k, " not exist")
		}(i)
	}
	wg.Wait()
	fmt.Println("elapsed time:", time.Now().Unix()-start, "seconds")

	fmt.Println()

	type CKey struct {
		k int
		c chan Val
	}
	cks := make(chan CKey)
	go func() { //broker
		var ks [10]int
		var cv [10]chan Val
		i := 0
		for i < 10 {
			select {
			case ck := <-cks:
				ks[i] = ck.k
				cv[i] = ck.c
				i++
			case <-time.After(100 * time.Millisecond):
				goto timeoutLbl
			}
		}
	timeoutLbl:
		vs := store.Get(ks[:i]...) //process data at once
		for i := 0; i < len(vs); i++ {
			cv[i] <- vs[i] //response result to all sender
		}
	}()
	wg.Add(len(kvs))
	start = time.Now().Unix()
	for i := 0; i < len(kvs); i++ {
		go func(i int) {
			defer wg.Done()
			k := kvs[i].k
			cv := make(chan Val)
			cks <- CKey{k, cv} //send key to broker that will be used to get the value
			v := <-cv //solution2(via broker): get value from store with given key (inside goroutine)
			close(cv)
			if v.ok {
				fmt.Println("routine:", i, " ,key:", k, " ,val:", v.val)
				return
			}
			fmt.Println("routine:", i, " ,key:", k, " not exist")
		}(i)
	}
	wg.Wait()
	fmt.Println("elapsed time:", time.Now().Unix()-start, "seconds")
}